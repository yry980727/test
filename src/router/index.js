import {createRouter,createWebHistory} from "vue-router";
const route=[
    {
        path: "/",
        redirect: "/main"
    },
    {
        path: "/login",
        name: "login",
        component: () => import("@/views/login/login.vue")
    },
    {
        path: "/main",
        name: "main",
        component: () => import("@/views/main/main.vue"),
        children: [
            {path:"board",component:import("@/views/main/board/board.vue")},
            {path:"account",component:import("@/views/main/account/account.vue")},
            {path:"project",component:import("@/views/main/project/project.vue")},
            {path:"team",component:import("@/views/main/team/team.vue")}
        ]
    },
    {
        path: "/:pathmatch(.*)*",
        name: "notfound",
        component: () => import("@/views/notfound/notfound.vue")
    },
    {
        path: "/register",
        name: "register",
        component: () => import("@/views/Register.vue")
    }
]
const router=createRouter({
    routes:route,
    history:createWebHistory()
})
export default router
