class LocalCache {
    setCashe(key, value) {
        window.localStorage.setItem(key, JSON.stringify(value))
    }

    getCashe(key) {
        const value = window.localStorage.getItem(key)
        if (value) return JSON.parse(value)
    }

    deleteCashe(key) {
        window.localStorage.removeItem(key)
    }
}

export default new LocalCache()
