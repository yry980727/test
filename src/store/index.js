import { createStore } from 'vuex'
import login from "@/store/login/login";

export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    login
  }
})
